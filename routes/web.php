<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('hello');
});


Route::get('/welcome', function () {
    return view('welcome');
});



Route::get('/about', function () {
    return view('aboutus');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/foo',function (){
    return "Hello World!";
});



Route::get('/student',function (){
    return view('Students/student');
});



Route::get('/student/{id}/{name?}',function ($id,$name=NULL){
    return view('Students.student',compact('id','name'));
});





Route::get('/students','StudentController@index');




Route::get('/student/{id}/{name?}','StudentController@view');







Route::get('/BookTitle/create',function (){

    return view('Book_Title/create');
} )->name('BookTitleCreate');

Route::post('/BookTitle/store', ['uses'=>'BookTitleController@store']);
Route::get('/BookTitle/index', 'BookTitleController@index')->name('index');
Route::get('/BookTitle/view/{id}', ['uses'=>'BookTitleController@view']);
Route::get('/BookTitle/edit/{id}', 'BookTitleController@edit');
Route::post('/BookTitle/update', ['uses'=>'BookTitleController@update']);
Route::get('/BookTitle/delete/{id}', ['uses'=>'BookTitleController@delete']);
Route::post('BookTitle/search_result', function(){
    $path = 'BookTitle/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/BookTitle/search/{keyword}', ['uses'=>'BookTitleController@search']);
Route::get('/BookTitle/search/',function (){
    return redirect()->route('index');
});

Route::get('/BookTitle/',function (){
    return redirect()->route('index');
});





Route::get('/Birthday/create',function (){

    return view('Birth_day/create');
} )->name('BirthdayCreate');

Route::post('/Birthday/store', ['uses'=>'BirthdayController@store']);
Route::get('/Birthday/index', 'BirthdayController@index')->name('index');
Route::get('/Birthday/view/{id}', ['uses'=>'BirthdayController@view']);
Route::get('/Birthday/edit/{id}', 'BirthdayController@edit');
Route::post('/Birthday/update', ['uses'=>'BirthdayController@update']);
Route::get('/Birthday/delete/{id}', ['uses'=>'BirthdayController@delete']);
Route::post('Birthday/search_result', function(){
    $path = 'Birthday/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/Birthday/search/{keyword}', ['uses'=>'BirthdayController@search']);
Route::get('/Birthday/search/',function (){
    return redirect()->route('index');
});

Route::get('/Birthday/',function (){
    return redirect()->route('index');
});




Route::get('/City/create',function (){
    return view('City/create');
} )->name('CityCreate');

Route::post('/City/store', ['uses'=>'CityController@store']);
Route::get('/City/index', 'CityController@index')->name('index');
Route::get('/City/view/{id}', ['uses'=>'CityController@view']);
Route::get('/City/edit/{id}', 'CityController@edit');
Route::post('/City/update', ['uses'=>'CityController@update']);
Route::get('/City/delete/{id}', ['uses'=>'CityController@delete']);

Route::post('City/search_result', function(){
    $path = 'City/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/City/search/{keyword}', ['uses'=>'CityController@search']);
Route::get('/City/search/',function (){
    return redirect()->route('index');
});
Route::get('/City/',function (){
    return redirect()->route('index');
});




Route::get('/Email/create',function (){

    return view('Email/create');
} )->name('EmailCreate');

Route::post('/Email/store', ['uses'=>'EmailController@store']);
Route::get('/Email/index', 'EmailController@index')->name('index');
Route::get('/Email/view/{id}', ['uses'=>'EmailController@view']);
Route::get('/Email/edit/{id}', 'EmailController@edit');
Route::post('/Email/update', ['uses'=>'EmailController@update']);
Route::get('/Email/delete/{id}', ['uses'=>'EmailController@delete']);

Route::post('Email/search_result', function(){
    $path = 'Email/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/Email/search/{keyword}', ['uses'=>'EmailController@search']);
Route::get('/Email/search/',function (){
    return redirect()->route('index');
});
Route::get('/Email/',function (){
    return redirect()->route('index');
});


Route::get('/Gender/create',function (){

    return view('Gender/create');
} )->name('GenderCreate');

Route::post('/Gender/store', ['uses'=>'GenderController@store']);
Route::get('/Gender/index', 'GenderController@index')->name('index');
Route::get('/Gender/view/{id}', ['uses'=>'GenderController@view']);
Route::get('/Gender/edit/{id}', 'GenderController@edit');
Route::post('/Gender/update', ['uses'=>'GenderController@update']);
Route::get('/Gender/delete/{id}', ['uses'=>'GenderController@delete']);

Route::post('Gender/search_result', function(){
    $path = 'Gender/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/Gender/search/{keyword}', ['uses'=>'GenderController@search']);
Route::get('/Gender/search/',function (){
    return redirect()->route('index');
});
Route::get('/Gender/',function (){
    return redirect()->route('index');
});



Route::get('/Hobbies/create',function (){

    return view('Hobbies/create');
} )->name('HobbiesCreate');

Route::post('/Hobbies/store', ['uses'=>'HobbiesController@store']);
Route::get('/Hobbies/index', 'HobbiesController@index')->name('index');
Route::get('/Hobbies/view/{id}', ['uses'=>'HobbiesController@view']);
Route::get('/Hobbies/edit/{id}', 'HobbiesController@edit');
Route::post('/Hobbies/update', ['uses'=>'HobbiesController@update']);
Route::get('/Hobbies/delete/{id}', ['uses'=>'HobbiesController@delete']);

Route::post('Hobbies/search_result', function(){
    $path = 'Hobbies/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/Hobbies/search/{keyword}', ['uses'=>'HobbiesController@search']);
Route::get('/Hobbies/search/',function (){
    return redirect()->route('index');
});
Route::get('/Hobbies/',function (){
    return redirect()->route('index');
});



Route::get('/ProfilePicture/create',function (){

    return view('Profile_Picture/create');
} )->name('ProfilePictureCreate');

Route::post('/Profile_Picture/store', ['uses'=>'ProfilePictureController@store']);
Route::get('/ProfilePicture/index', 'ProfilePictureController@index')->name('index');
Route::get('/ProfilePicture/view/{id}', ['uses'=>'ProfilePictureController@view']);
Route::get('/ProfilePicture/edit/{id}', 'ProfilePictureController@edit');
Route::post('/ProfilePicture/update', ['uses'=>'ProfilePictureController@update']);
Route::get('/ProfilePicture/delete/{id}', ['uses'=>'ProfilePictureController@delete']);

Route::post('ProfilePicture/search_result', function(){
    $path = 'ProfilePicture/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/ProfilePicture/search/{keyword}', ['uses'=>'ProfilePictureController@search']);
Route::get('/ProfilePicture/search/',function (){
    return redirect()->route('index');
});
Route::get('/ProfilePicture/',function (){
    return redirect()->route('index');
});




Route::get('/SummaryOfOrganization/create',function (){

    return view('Summary_Of_Organization/create');
} )->name('SummaryOfOrganizationCreate');

Route::post('/Summary_Of_Organization/store', ['uses'=>'SunmmaryOfOrganizationController@store']);
Route::get('/SummaryOfOrganization/index', 'SummaryOfOrganizationController@index')->name('index');
Route::get('/SummaryOfOrganization/view/{id}', ['uses'=>'SummaryOfOrganizationController@view']);
Route::get('/SummaryOfOrganization/edit/{id}', 'SummaryOfOrganizationController@edit');
Route::post('/SummaryOfOrganization/update', ['uses'=>'SummaryOfOrganizationController@update']);
Route::get('/SummaryOfOrganization/delete/{id}', ['uses'=>'SummaryOfOrganizationController@delete']);

Route::post('SummaryOfOrganization/search_result', function(){
    $path = 'SummaryOfOrganization/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/SummaryOfOrganization/search/{keyword}', ['uses'=>'SummaryOfOrganizationController@search']);
Route::get('/SummaryOfOrganization/search/',function (){
    return redirect()->route('index');
});
Route::get('/SummaryOfOrganization/',function (){
    return redirect()->route('index');
});

