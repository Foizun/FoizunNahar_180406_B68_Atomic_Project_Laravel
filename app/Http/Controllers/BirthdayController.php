<?php

namespace App\Http\Controllers;

use App\Birthday;
use Illuminate\Http\Request;

class BirthdayController extends Controller
{
    public function store(){

       $objModel = new Birthday();

        $objModel->user_name = $_POST['user_name'];
        $objModel->birthday = $_POST['birthday'];


        $status = $objModel->save();

        var_dump($status);
        return redirect()->route('BirthdayCreate');
    }


    public function index(){

        $objBirthdayModel = new Birthday();

        $allData = $objBirthdayModel->paginate(5);


        return view("Birth_day/index",compact('allData'));

    }

    public function view($id){


        $objBirthdayModel = new Birthday();


        $oneData = $objBirthdayModel->find($id);

        return view('Birth_day/view',compact('oneData'));

    }




    public function edit($id){


        $objBirthdayModel = new Birthday();

        $oneData = $objBirthdayModel->find($id);

        return view('Birth_day/edit',compact('oneData'));
    }
    public function update(){


        $objBirthdayModel = new Birthday();

        $oneData = $objBirthdayModel->find($_POST['id']);
        $oneData->user_name = $_POST["user_name"];
        $oneData->birthday = $_POST["birthday"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('index');


    }



    public function delete($id){


        $objBirthdayModel = new Birthday();

        $status = $objBirthdayModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('index');

    }
    public function search($keyword){



        $objBirthdayModel = new Birthday();

        $searchResult =  $objBirthdayModel
            ->where("user_name","LIKE","%$keyword%")
            ->orwhere("birthday","LIKE","%$keyword%")
            ->paginate(5);


        return view('Birth_day/search_result',compact('searchResult')) ;

    }

}
