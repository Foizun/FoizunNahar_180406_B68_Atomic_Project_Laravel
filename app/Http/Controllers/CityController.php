<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function store(){
     $objModel =  new City();

        $objModel->user_name = $_POST['user_name'];
        $objModel->city = $_POST['city'];


        $status = $objModel->save();


        return redirect()->route('CityCreate');
    }
    public function index(){

        $objCityModel = new City();

        $allData = $objCityModel->paginate(5);


        return view("City/index",compact('allData'));

    }



    public function view($id){


        $objCityModel = new City();


        $oneData = $objCityModel->find($id);

        return view('City/view',compact('oneData'));

    }
    public function edit($id){


        $objCityModel = new City();

        $oneData = $objCityModel->find($id);

        return view('City/edit',compact('oneData'));
    }




    public function update(){


        $objCityModel = new City();

        $oneData = $objCityModel->find($_POST['id']);
        $oneData->user_name = $_POST["user_name"];
        $oneData->city = $_POST["city"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('index');


    }



    public function delete($id){


        $objCityModel = new City();

        $status = $objCityModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('index');

    }


    public function search($keyword){



        $objCityModel = new City();

        $searchResult =  $objCityModel
            ->where("user_name","LIKE","%$keyword%")
            ->orwhere("city","LIKE","%$keyword%")
            ->paginate(5);


        return view('City/search_result',compact('searchResult')) ;

    }
}
