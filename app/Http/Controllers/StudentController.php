<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index(){


        $allStudents = [

            [
                "name" => "Mr. X",
                "roll" => 44,
                "gender" => "Male"
            ],

            [
                "name" => "Mrs. Y",
                "roll" => 30,
                "gender" => "Female"
            ],

            [
                "name" => "Mr. Z",
                "roll" => 24,
                "gender" => "Male"
            ],

        ];

        return view('Students/students', compact('allStudents'));



    }// end of index()
    
    
    
    public function view($id,$name){

      return view('Students/student', compact('id','name'));


    }// end of view()
    
    
    
}
