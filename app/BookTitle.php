<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTitle extends Model
{


    public $timestamps = false;

    protected $fillable = [
        'book_title', 'author_name'
    ];
}
