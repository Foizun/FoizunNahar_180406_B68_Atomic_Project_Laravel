@extends('master')


@section('title','City - Single City')


@section('content')


    <h1 style="text-align: center"> Single City Information: </h1>
    <table class="table table-bordered">

        <tr> <td><b>User Name</b> </td> <td> {!! $oneData['user_name'] !!} </td> </tr>
        <tr> <td> <b>City </b></td> <td>{!! $oneData['city'] !!} </td> </tr>

    </table>


@endsection