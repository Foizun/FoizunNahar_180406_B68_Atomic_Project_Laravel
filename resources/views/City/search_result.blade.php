@extends('master')


@section('title','City - Active List')


@section('content')



    <div class="container">

            <div class="navbar">

                    <a href="create"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>
            </div>

            Total: {!! $searchResult->total() !!} User Name(s) <br>

            Showing: {!! $searchResult->count() !!} User Name(s) <br>

            {!! $searchResult->links() !!}
            <table class="table table-bordered table table-striped" >

                    <th>User Name</th>
                    <th>City</th>
                    <th>Action Buttons</th>

                    @foreach($searchResult as $oneData)

                            <tr>

                                    <td>  {!! $oneData['user_name'] !!} </td>
                                    <td>  {!! $oneData['city'] !!} </td>
                               
                                    <td>
                                        <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                                        <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                                        <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                                    </td>

                            </tr>


                    @endforeach


            </table>
            {!! $searchResult->links() !!}
    </div>


@endsection
