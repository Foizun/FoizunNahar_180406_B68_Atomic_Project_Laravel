@extends('../master')

@section('title','City - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> City - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/City/store']) !!}

            {!! Form::label('user_name','User Name:') !!}
            {!! Form::text('user_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('city','City:') !!}
            {!! Form::select('city', ['Dhaka' => 'Dhaka', 'Chittagong' => 'Chittagong','Comilla' => 'Comilla',
            'Rajshahi' => 'Rajshahi']) !!}
            <br>
            <br>
            <br><br><br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
