@extends('master')


@section('title','Email - Single Email')


@section('content')


    <h1 style="text-align: center"> Single Email Information: </h1>
    <table class="table table-bordered">

        <tr> <td><b>User Name</b> </td> <td> {!! $oneData['user_name'] !!} </td> </tr>
        <tr> <td> <b>Email </b></td> <td>{!! $oneData['email'] !!} </td> </tr>

    </table>


@endsection