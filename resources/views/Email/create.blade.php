@extends('../master')

@section('title','Email - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Email - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Email/store']) !!}

            {!! Form::label('user_name','User Name:') !!}
            {!! Form::text('user_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('email','Email:') !!}
            {!! Form::text('email','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
