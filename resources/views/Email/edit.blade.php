@extends('../master')

@section('title','Email - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Email - Edit Form</h3>
            <hr>

    {!! Form::open(['url'=>'/Email/update']) !!}

    {!! Form::label('user_name','User Name:') !!}
    {!! Form::text('user_name',$oneData['user_name'],['class'=>'form-control', 'required'=>'required']) !!}

    <br>

    {!! Form::label('Email','Email:') !!}
    {!! Form::text('email',$oneData['email'], ['class'=>'form-control', 'required'=>'required']) !!}
            <br>


     {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection