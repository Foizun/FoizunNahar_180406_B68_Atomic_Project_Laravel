@extends('master')


@section('title','Book Title - Single Book')


@section('content')


    <h1 style="text-align: center"> Single Book Information: </h1>
    <table class="table table-bordered">

        <tr> <td><b>Book Title</b> </td> <td> {!! $oneData['book_title'] !!} </td> </tr>
        <tr> <td> <b>Author </b></td> <td>{!! $oneData['author_name'] !!} </td> </tr>

    </table>


@endsection