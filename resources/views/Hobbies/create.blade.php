@extends('../master')

@section('title','Hobbies - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Hobbies - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Hobbies/store']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('hobbies','Hobbies:') !!}<br>

            {!! Form::checkbox('hobbies[]', 'sleeping' ) !!}
            {!! Form::label('hobbies','Sleeping') !!}
            <br>
            {!! Form::checkbox('hobbies[]', 'Php coding' ) !!}
            {!! Form::label('hobbies','Php coding') !!}
          <br>
            {!! Form::checkbox('hobbies','HTML coding') !!}
            {!! Form::label('hobbies','HTML coding') !!}
            <br>
            {!! Form::checkbox('hobbies[]', 'C coding' ) !!}
            {!! Form::label('hobbies[]', 'C coding' ) !!}
            <br>
            {!! Form::checkbox('hobbies[]', 'C++ Coding' ) !!}
            {!! Form::label('hobbies','C++ Coding') !!}

            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
