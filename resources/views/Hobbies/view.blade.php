@extends('master')


@section('title','Hobbies - Single Hobby')


@section('content')


    <h1 style="text-align: center"> Single Hobbies Information: </h1>
    <table class="table table-bordered">

        <tr> <td><b>Name</b> </td> <td> {!! $oneData['name'] !!} </td> </tr>
        <tr> <td> <b>Hobbies </b></td> <td>{!! $oneData['hobbies'] !!} </td> </tr>

    </table>


@endsection