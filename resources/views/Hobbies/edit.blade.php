@extends('../master')

@section('title','Hobbies - Edit Form')




@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Hobbies - Edit Form</h3>
            <hr>

    {!! Form::open(['url'=>'/Hobbies/update']) !!}

    {!! Form::label('name','Name:') !!}
    {!! Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']) !!}

    <br>

    {!! Form::label('Hobbies','Hobbies:') !!}
    {!! Form::text('hobbies',$oneData['hobbies'], ['class'=>'form-control', 'required'=>'required']) !!}
            <br>


     {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

    {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

    {!! Form::close() !!}

    </div>
</div>

@endsection