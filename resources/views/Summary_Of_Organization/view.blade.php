@extends('master')


@section('title','Summary Of Organization - Single Summary Of Organization')


@section('content')


    <h1 style="text-align: center"> Single Summary Of Organization Information: </h1>
    <table class="table table-bordered">

        <tr> <td><b>Name</b> </td> <td> {!! $oneData['name'] !!} </td> </tr>
        <tr> <td> <b>Summary Of Organization</b></td> <td>{!! $oneData['summary'] !!} </td> </tr>

    </table>


@endsection