@extends('master')


@section('title','Profile Picture - Single Profile Picture')


@section('content')


    <h1 style="text-align: center"> Single Profile Picture Information: </h1>
    <table class="table table-bordered">

        <tr> <td><b>Name</b> </td> <td> {!! $oneData['name'] !!} </td> </tr>
        <tr> <td> <b>Profile Picture</b></td> <td>{!! $oneData['profile_picture'] !!} </td> </tr>

    </table>


@endsection