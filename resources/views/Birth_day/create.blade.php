@extends('../master')

@section('title','Birth Day - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Birth Day - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Birthday/store']) !!}

            {!! Form::label('user_name','User Name:') !!}
            {!! Form::text('user_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('birthday','Birthday:') !!}
            {!! Form::date('birthday','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection