@extends('master')


@section('title','Gender - Single Gender')


@section('content')


    <h1 style="text-align: center"> Single Gender Information: </h1>
    <table class="table table-bordered">

        <tr> <td><b>User Name</b> </td> <td> {!! $oneData['user_name'] !!} </td> </tr>
        <tr> <td> <b>Gender </b></td> <td>{!! $oneData['gender'] !!} </td> </tr>

    </table>


@endsection