@extends('../master')

@section('title','Gender - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Gender - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Gender/store']) !!}

            {!! Form::label('user_name','User Name:') !!}
            {!! Form::text('user_name','',['class'=>'form-control', 'required'=>'required']) !!}


            <br>

            {!! Form::label('gender','Gender:') !!}
            <br>
            {!! Form::radio('gender', 'male') !!}
            {!! Form::label('gender','Male') !!}
            <br>

            {!! Form::radio('gender', 'female') !!}
            {!! Form::label('gender','Female') !!}
            <br>
            {!! Form::radio('gender','other') !!}
            {!! Form::label('gender','Other') !!}


            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection